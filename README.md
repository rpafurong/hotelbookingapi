 **Setup and Configuration**
 

 **Cloning this project to your local machine :**

 Open you terminal or command prompt or powershell :

 **Change Directory to your desired folder :**
 
 `cd yourDesiredFolder`
 
 **Check if git is installed on your machine :**
 
 ` git --version `
 
 If it displays something similar to this :
 
 git version 2.23.0.windows.1
 
 Your good to go.
 
 **For more info on git :**
 
 https://www.atlassian.com/git/tutorials/install-git
 
 **Clone the project :**
 
 ` git clone https://gitlab.com/rpafurong/hotelbookingapi.git `
 
 **Change Directory to the project :**
 
 ` cd hotelbookingapi `
 
 **Check Python 3 if its installed in your system :**
 
 ` python -V `
 
 If it displays something similar to this :
 
 Python 3.7.4
 
 Your fine but also get in the habit of check the python version on the PipFile of the project if it is available.
 If it is and you have a python already installed.. you can try to change it with your python version and see if it works.
 Else follow this links accordingly.
 
 **For more info on Python :**
 
 Mac OSX :
 
 https://wsvincent.com/install-python3-mac/
 
 Windows : 
 
 https://www.python.org/
 
 Others :
 
 https://www.python.org/download/other/
 
 **Make sure latest pip is also installed on the machine**
 
 **For more info on pip :**
 
 https://pip.pypa.io/en/stable/installing/
 
 **Upgrading pip to the latest version**
 
 If you're on **Linux** like **Ubuntu** :
 
 ` pip install --upgrade pip `
 
 If you're on **Windows** :
 
 ` python -m pip install --upgrade pip `
 
 **Install virtual environment for isolation purposes in case something when wrong :**
 
 ` pip install --upgrade pipenv `
 
 
 
 **Using the project**
 
 Please make sure your on the /hotelbookingapi folder on your command prompt or terminal.
 
 **Start your virtual environment**
 
 ` pipenv shell `
 
 **Install requirements**
 
 ` pip install -r requirements.txt `
 
 **Checked installed packages**
 
 ` pip freeze `
 
 **Start django's builtin server to view the project in your web browser**
 
 Change to /src folder first:
 
 ` cd src `

 Create admin user:
 
 ` python manage.py createsuperuser `
 
 Start the django server :
 
 ` python manage.py runserver `
 
 **or with my preferred way to avoid port or ip address issues :**
 
 ` python manage.py runserver 0.0.0.0:8000 `

 You can now visit the site by entering the following in your browser :

 `localhost:8000`

 **or**

 `127.0.0.1:8000`

 Login to your dashboard using your admin account you created above.

 That's it.
 
 That's all there is to it.

 Enjoy =)

 
