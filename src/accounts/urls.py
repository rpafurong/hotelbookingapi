from django.contrib import admin
from django.urls import include, path

from .views import LoginPageView, DashboardPageView, LogoutPageView

urlpatterns = [
    path('login/', LoginPageView.as_view(), name='login-page'),
    path('dashboard/', DashboardPageView.as_view(), name='dashboard-page'),
    path('logout/', LogoutPageView.as_view(), name='logout-page'),
]