from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.db.models import Q

from hotel.models import Room
from reservation.models import Reservation

# Create your views here.

class LoginPageView(LoginView):
    template_name = 'accounts/login.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('dashboard-page')
        return super(LoginPageView, self).dispatch(request, *args, **kwargs)
        
class DashboardPageView(LoginRequiredMixin, ListView):
    model = Room
    template_name = 'accounts/dashboard/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get('search')

        if query:
            object_list = Room.objects.filter(
                Q(hotel__city__icontains=query)
            )
            object_list["reservation"] = Reservation.objects.filter(
                Q(check_in__icontains=query),
                Q(check_out__icontains=query)
            )
        else:
            object_list = None
    
        print(query)
        
        context = {
            'object_list': object_list
        }
        return context

class LogoutPageView(LogoutView):
    template_name = 'accounts/logout.html'
    