from rest_framework import serializers

from hotel.models import Hotel, Room

class RoomSerializer(serializers.ModelSerializer):
    facilities = serializers.StringRelatedField(many=True)
    class Meta:
        model = Room
        fields = [
            'id',
            'description',
            'room_type',
            'adult_capacity',
            'rate',
            'created',
            'modified',
            'facilities'
        ]

class HotelSerializer(serializers.ModelSerializer):
    # facilities = serializers.StringRelatedField(many=True)
    class Meta:
        model = Hotel
        fields = [
            'id',
            'name'
        ]