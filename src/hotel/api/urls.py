from django.urls import path

from .views import HotelAPIView, RoomAPIView

urlpatterns = [
    path('rooms/', RoomAPIView.as_view(), name='rooms-list-api'),
    path('hotels/', HotelAPIView.as_view(), name='hotels-list-api')
]