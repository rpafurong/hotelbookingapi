from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from rest_framework import generics
from rest_framework import filters

from hotel.models import Hotel, Room
from .serializers import HotelSerializer, RoomSerializer

@method_decorator(login_required, name='dispatch')
class RoomAPIView(generics.ListAPIView):
    model = Room
    serializer_class = RoomSerializer
    search_fields = [
        'description',
        'rate', 
        'adult_capacity', 
        'room_type', 
        'facilities__name'
    ]
    filter_backends = (filters.SearchFilter,)
    queryset = Room.objects.all()




@method_decorator(login_required, name='dispatch')
class HotelAPIView(generics.ListAPIView):
    model = Hotel
    serializer_class = HotelSerializer
    search_fields = [
        'name'
    ]
    filter_backends = (filters.SearchFilter,)
    queryset = Hotel.objects.all()
    