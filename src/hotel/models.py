from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Hotel(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=120)
    phone_number = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    photo = models.ImageField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Facility(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Facilities"

    def __str__(self):
        return self.name

class Room(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    room_type = models.CharField(max_length=120)
    facilities = models.ManyToManyField(Facility)
    adult_capacity = models.IntegerField(default=0)
    rate = models.FloatField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description