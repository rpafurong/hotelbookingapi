from django.shortcuts import redirect
from django.views.generic import TemplateView

class HomePageView(TemplateView):
    template_name = 'homepage.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('dashboard-page')
        return super(HomePageView, self).dispatch(request, *args, **kwargs)